package com.jtsmart.dao.repository.face;

import com.jtsmart.model.pojo.bus.Apply;
import com.jtsmart.model.pojo.face.FacePeople;
import com.jtsmart.model.pojo.face.FaceRecognitionMachine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface FaceRecognitionMachineRepo extends JpaRepository<FaceRecognitionMachine, Long>, JpaSpecificationExecutor<FaceRecognitionMachine> {



    List<FaceRecognitionMachine> findAll();

    


    FaceRecognitionMachine findById(int id);

}