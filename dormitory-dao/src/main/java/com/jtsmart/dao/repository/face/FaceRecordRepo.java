package com.jtsmart.dao.repository.face;

import com.jtsmart.model.pojo.bus.Apply;
import com.jtsmart.model.pojo.face.FacePeople;
import com.jtsmart.model.pojo.face.FaceRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface FaceRecordRepo extends JpaRepository<FaceRecord, Integer>, JpaSpecificationExecutor<FaceRecord> {



    List<FaceRecord> findAll();




    FaceRecord findById(int id);

}