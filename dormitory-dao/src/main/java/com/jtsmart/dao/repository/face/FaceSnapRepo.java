package com.jtsmart.dao.repository.face;

import com.jtsmart.model.pojo.bus.Apply;
import com.jtsmart.model.pojo.face.FaceRecord;
import com.jtsmart.model.pojo.face.FaceSnap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface FaceSnapRepo extends JpaRepository<FaceSnap, Long>, JpaSpecificationExecutor<FaceSnap> {



    List<FaceSnap> findAll();




    FaceSnap findById(int id);

}