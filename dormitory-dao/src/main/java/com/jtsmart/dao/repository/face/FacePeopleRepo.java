package com.jtsmart.dao.repository.face;

import com.jtsmart.model.pojo.bus.Apply;
import com.jtsmart.model.pojo.face.FacePeople;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface FacePeopleRepo extends JpaRepository<FacePeople, Long>, JpaSpecificationExecutor<FacePeople> {



    List<FacePeople> findAll();




    FacePeople findById(int id);

}