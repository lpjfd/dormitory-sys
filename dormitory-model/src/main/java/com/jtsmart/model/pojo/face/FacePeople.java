package com.jtsmart.model.pojo.face;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author lpj
 * @date 2025/3/4
 * @apiNote
 */
@Data
@Entity
@Table(name = "t_face_people")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class FacePeople implements Serializable {

    @Id // Specifies the primary key of the entity.
    @GeneratedValue(strategy = GenerationType.IDENTITY) // Provides for the specification of generation strategies for the values of primary keys.
    private Long id;

    @Column(name = "device_id")
    @JsonProperty("DeviceID")
    private Integer deviceId;

    @Column(name = "id_card_idno")
    @JsonProperty("IDCard_Idno")
    private String idCardIdno;

    @Column(name = "id_card_name")
    @JsonProperty("IDCard_Idno")
    private String idCardName;

    @Column(name = "id_card_gender")
    @JsonProperty("IDCard_Idno")
    private Integer idCardGender;

    @Column(name = "id_card_nation")
    @JsonProperty("IDCard_Nation")
    private Integer idCardNation;

    @Column(name = "id_card_birthday")
    @JsonProperty("IDCard_Birthday")
    private String idCardBirthday;

    @Column(name = "id_card_address")
    @JsonProperty("IDCard_Address")
    private String idCardAddress;

    @Column(name = "id_card_id_issue")
    @JsonProperty("IDCard_Idissue")
    private String idCardIdIssue;

    @Column(name = "id_card_id_period")
    @JsonProperty("IDCard_Idperiod")
    private String idCardIdPeriod;

    @Column(name = "id_card_photo")
    @JsonProperty("IDCard_photo")
    private String idCardPhoto;
}
