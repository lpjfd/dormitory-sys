package com.jtsmart.model.pojo.face;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
/**
 * @author lpj
 * @date 2025/3/4
 * @apiNote
 */
@Data
@Entity
@Table(name = "t_face_snap")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class FaceSnap implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "device_id")
    @JsonProperty("DeviceId")
    private Integer deviceId;

    @Column(name = "picture_type")
    @JsonProperty("PictureType")
    private Integer pictureType; // 0: 比对失败, 1: 比对成功黑名单, 2: 比对成功白名单, 3：身份证+人脸比对失败, 4：身份证+人脸比对成功

    @Column(name = "temperature")
    @JsonProperty("Temperature")
    private Double temperature;

    @Column(name = "temperature_alarm", length = 255)
    @JsonProperty("TemperatureAlarm")
    private String temperatureAlarm; // 0：没超过；1：超过

    @Column(name = "snap_channel")
    @JsonProperty("SnapChannel")
    private Integer snapChannel;

    @Column(name = "sendintime")
    @JsonProperty("SendinTime")
    private LocalDateTime sendinTime; // 是否及时推送信息, 0:未及时推送（推送时间大于抓拍时间10秒）1:及时推送

    @Column(name = "direction")
    @JsonProperty("Direction")
    private Integer direction; // 出入口方向 0：单向 1：入口 2：出口

    @Column(name = "sanp_pic", columnDefinition = "TEXT")
    @JsonProperty("SanpPic")
    private String sanpPic; // 抓拍的人脸图片的base64编码数据

    @Column(name = "create_time")
    @JsonProperty("CreateTime")
    private LocalDateTime createTime; // 抓拍记录时间
}
