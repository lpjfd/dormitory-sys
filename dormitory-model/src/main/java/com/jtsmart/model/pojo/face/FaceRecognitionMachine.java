package com.jtsmart.model.pojo.face;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author lpj
 * @date 2025/3/4
 * @apiNote
 */
@Data
@Entity
@Table(name = "t_face_recognition_machine")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class FaceRecognitionMachine implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // Provides for the specification of generation strategies for the values of primary keys.
    private Long id;

    @Column(name = "device_id")
    private Integer deviceId;

    @Column(name = "name")
    private String name;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "create_time")
    private LocalDateTime createTime;
}
