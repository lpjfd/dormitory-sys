package com.jtsmart.model.pojo.face;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author lpj
 * @date 2025/3/4
 * @apiNote
 */

@Data
@Entity
@Table(name = "t_face_record")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class FaceRecord implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "device_id")
    @JsonProperty("DeviceID")
    private Integer deviceId;

    @Column(name = "person_id")
    @JsonProperty("PersonID")
    private Integer personId;

    @Column(name = "create_time")
    @JsonProperty("CreateTime")
    private LocalDateTime createTime;

    @Column(name = "similarity1")
    @JsonProperty("Similarity1")
    private Float similarity1;

    @Column(name = "similarity2")
    @JsonProperty("Similarity2")
    private Float similarity2;

    @Column(name = "verify_status")
    @JsonProperty("VerifyStatus")
    private Integer verifyStatus;

    @Column(name = "verfy_type")
    @JsonProperty("VerifyType")
    private Integer verfyType;

    @Column(name = "person_type")
    @JsonProperty("PersonType")
    private Integer personType;

    @Column(name = "name")
    @JsonProperty("Name")
    private String name;

    @Column(name = "gender")
    @JsonProperty("Gender")
    private Integer gender;

    @Column(name = "nation")
    @JsonProperty("Nation")
    private Integer nation;

    @Column(name = "card_type")
    @JsonProperty("CardType")
    private Integer cardType;

    @Column(name = "id_card")
    @JsonProperty("IdCard")
    private String idCard;

    @Column(name = "birthday")
    @JsonProperty("Birthday")
    private String birthday;

    @Column(name = "native")
    @JsonProperty("Native")
    private String nativePlace; // 'native' 是关键字，所以这里改名为 nativePlace

    @Column(name = "address")
    @JsonProperty("Address")
    private String address;

    @Column(name = "notes")
    @JsonProperty("Notes")
    private String notes;

    @Column(name = "tempvalid")
    @JsonProperty("TempValid")
    private Integer tempValid;

    @Column(name = "access_channel")
    @JsonProperty("AccessChannel")
    private Integer accessChannel;

    @Column(name = "temperature")
    @JsonProperty("Temperature")
    private Double temperature;

    @Column(name = "temperature_alarm")
    @JsonProperty("TemperatureAlarm")
    private Integer temperatureAlarm;

    @Column(name = "sendintime")
    @JsonProperty("SendinTime")
    private Integer sendinTime;

    @Column(name = "direction")
    @JsonProperty("Direction")
    private Integer direction;

    @Column(name = "sanp_pic", columnDefinition = "TEXT")
    @JsonProperty("SanpPic")
    private String sanpPic;

    @Column(name = "registered_pic", columnDefinition = "TEXT")
    @JsonProperty("RegisteredPic")
    private String registeredPic;

    @Column(name = "push_type")
    @JsonProperty("PushType")
    private Integer pushType;
}